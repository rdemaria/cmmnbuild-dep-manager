'''CommonBuild Dependency Manager

Copyright (c) CERN 2015-2020

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Authors:
    T. Levens       <tom.levens@cern.ch>
    R. De Maria     <riccardo.de.maria@cern.ch>
    M. Hostettler   <michi.hostettler@cern.ch>
    P. Elson        <philip.elson@cern.ch>
'''

import atexit
from contextlib import contextmanager
import dataclasses
import functools
import importlib
import json
import logging
import os
import pathlib
import pickle
import pprint
import re
import shutil
import site
import sys
import typing
import zipfile

import entrypoints
import jpype

from .jvm import _manager as _jvm_manager
from .jvm._startup_arguments import join_class_path as _join_class_path
from .jvm import _memory


#: The path to the modules.json file found *inside* the package. Other
#: locations exist, see :class:`Manager`.
PKG_MODULES_JSON = pathlib.Path(__file__).parent / 'modules.json'


def _requires_resolver(fn):
    """A decorator to ensure that a resolver is setup before the function is run."""
    @functools.wraps(fn)
    def wrapped(self, *args, **kwargs):
        if not getattr(self, '_resolver', None):
            self._setup_resolver()
        return fn(self, *args, **kwargs)
    return wrapped


DependencyType = typing.Union[str, typing.Dict]


class Manager:
    #: The :class:`~cmmnbuild_dep_manager.jvm._manager.JVMManager` instance which influences JPype JVM startup in this class (class attribute).
    JVM = _jvm_manager.JVMManager()

    # Workaround OpenBLAS issue at https://github.com/jpype-project/jpype/issues/808 by setting
    # a minimum stack size of 4MB.
    JVM.startup_arguments.stack_size = '4m'

    # Ensure that keyboard interrupts are propagated correctly back to Python (and ultimately Qt).
    JVM.startup_arguments.disable_signal_handling = True

    # Pick a larger than default maximum heap size, as we are typically doing heavier
    # numerical operations through the JVM (particularly libraries such as PyTimber).
    JVM.startup_arguments.max_heap_size = str(_memory.choose_heap_max(
        _memory.machine_memory())
    )

    def __init__(self, pkg=None, lvl=None):
        # Note: There are two patterns being used with Manager:
        #  1: Manager() - a thing that can be manipulated and resolved explicitly
        #  2: Manager(pkg=...) - a thing that will be resolved automatically during
        #                        construction.
        self.log = logging.getLogger(__package__)
        if lvl is not None:
            self.log.setLevel(lvl)
        # Check if `logging.basicConfig()` has been called to give the
        # root logger a handler. If yes, we defer to it. If not, we
        # create our own handler to print regardless. In that case, we
        # set `propagate` to False to prevent messages from bubbling up.
        # Otherwise, messages might be printed twice if `basicConfig()`
        # is called later.
        if not self.log.hasHandlers():
            self.log.addHandler(logging.StreamHandler())
            self.log.propagate = False

        needs_installation = pkg is not None and not self.is_installed(pkg)
        if needs_installation:
            # Temporarily set the log level to notify the user that this could
            # take some time.
            with self._tmp_set_logging_level('INFO'):
                self.log.info(
                    'Package "{0}" is not yet set up - installing and '
                    'resolving JARs'.format(pkg))
                self.install(pkg)

    def set_logging_level(self, lvl):
        '''Set the logging level'''
        self.log.setLevel(lvl)

    @contextmanager
    def _tmp_set_logging_level(self, lvl):
        orig_level = self.log.level
        self.log.setLevel(lvl)
        yield
        self.log.setLevel(orig_level)

    @contextmanager
    def imports(self):
        '''Run a piece of code with the access to the Java universe through JPype imports.
        When the context ends, the original python import behavior is restored.
        '''
        old_sys_meta_path = list(sys.meta_path)
        old_modules = set(sys.modules.keys())

        if not jpype.isJVMStarted():
            self.start_jpype_jvm()

        # Enable the import hook (by importing the jpype.imports module).
        import jpype.imports as enabled_imports

        jpype.imports.registerDomain("cern")
        try:
            yield jpype
        finally:
            sys.meta_path = old_sys_meta_path
            added_modules = set(sys.modules.keys()) - old_modules
            for mod in added_modules:
                del sys.modules[mod]
            importlib.invalidate_caches()

    def help(self, method='__init__'):
        '''Print the docstring for a method'''
        # TODO: Deprecate this.
        print(getattr(self, method).__doc__)

    def jar_path(self):
        '''Return the directory containing the resolved jars'''
        return os.path.join(self._dir(), 'lib')

    def jars(self):
        '''Return a list of the resolved jars'''
        if os.path.isdir(self.jar_path()):
            return [
                os.path.join(self.jar_path(), f)
                for f in sorted(os.listdir(self.jar_path()))
                if f.lower().endswith('.jar')
            ]
        else:
            return []

    def class_list(self):
        '''List all classes in the resolved jars'''
        return sorted(self._class_data().keys())

    def class_hints(self, class_name):
        '''Print code hints for using a Java class from Python code

        Usage::

            mgr = cmmnbuild_dep_manager.Manager()
            jpype = mgr.start_jpype_jvm()
            print(mgr.class_hints('Fidel'))

        '''
        names = sorted(self.class_search(class_name))
        roots = set()
        for name in names:
            parts = name.split('.')
            cname = parts[-1]
            root = parts[0]
            if root not in roots:
                print('{0} = jpype.JPackage(\'{0}\')'.format(root))
                roots.add(root)
            print('{0} = {1}'.format(cname, name))

    def class_search(self, class_name):
        '''Search for Java classes by name'''
        return list(filter(lambda x: class_name in x, self.class_list()))

    def class_path(self, extra_jars=()):
        '''Returns a delimited string suitable for java.class.path'''
        # TODO: Deprecate this.
        jars = self.jars()
        jars.extend(extra_jars)
        return _join_class_path(jars)

    def _class_data(self):
        '''List all classes in the resolved jars'''
        classes = {}
        for jar in self.jars():
            jarname, jarversion = self._get_jarversion(jar)
            names = zipfile.ZipFile(jar).namelist()
            for name in names:
                if name.endswith('.class'):
                    classname = name[:-6].replace('/', '.')
                    classes.setdefault(classname, []).append((
                        jarname, jarversion
                    ))
        return classes

    def _get_jarversion(self, jar):
        filename = os.path.splitext(os.path.basename(jar))[0]
        matches = re.search(r'^(.+?)-(\d.*)', filename)

        # raise an exception when there is no match (usually no library version specified)
        if not matches:
            raise ValueError('Malformed jar name "{}"'.format(filename))

        return matches[1], matches[2]

    @_requires_resolver
    def class_doc(self, obj_or_string):
        '''Return URLs of the documentation and source code of a class

        Example:

        import pjlsa
        lsa=pjlsa.LSAClient()
        pjlsa.mgr.class_doc(lsa._contextService)

        '''
        # TODO: This is untested.
        if isinstance(obj_or_string, str):
            classname = obj_or_string
        elif hasattr(obj_or_string, 'getProxiedInterfaces'):
            classname = obj_or_string.getProxiedInterfaces()[0].__name__
        elif isinstance(obj_or_string, jpype._jclass._MetaClassForMroOverride):
            classname = obj_or_string.__name__
        else:
            classname = obj_or_string.__class__.__name__
        if classname not in self.class_list():
            classnames = self.class_search(classname)
            if not classnames:
                raise ValueError('Could not find class {0}'.format(classname))
        else:
            classnames = [classname]
        classnames = set(map(lambda x: x.split('$')[0], classnames))
        self._resolver.get_help(classnames, self._class_data())

    def jvm_required(self):
        """
        Ensure that the JVM is running with the appropriate class_path for
        this Manager.

        This method will resolve the required JARs if they have not already been resolved.

        Note that if the JVM is already running and wasn't started by the
        ``Manager.JVM`` instance (either directly or indirecly), an error will
        be raised.

        Note: It is only possible to control the Java class path *after* the
        Manager is resolved (yet before the JVM has started).
        A RuntimeError will be raised if the ``class_path``
        is already defined. For example:

        >>> mgr = Manager()
        >>> mgr.resolve()
        >>> mgr.JVM.startup_arguments.class_path.append('/path/to/another.JAR')
        >>> mgr.jvm_required()

        """
        if not self.is_resolved():
            if self.JVM.startup_arguments.class_path:
                raise RuntimeError(
                    "The JVM.startup_arguments.class_path can only be set "
                    "after the Manager is resolved."
                )
            self.resolve()

        # Only add the resolved JARs if there aren't already JARs setup.
        if not self.JVM.startup_arguments.class_path:
            self.JVM.startup_arguments.class_path.extend(self.jars())

        self.JVM.required()

    def start_jpype_jvm(self, extra_jars=()):
        """
        Starts a new JPype JVM with the appropriate class path.

        Note that this method does not resolve JARs before starting the JVM.

        """
        if not jpype.isJVMStarted():
            self.log.info(
                'starting a JPype JVM with {0} jars from {1}'.format(
                    len(self.jars()),
                    self.jar_path()
                )
            )
            if self.JVM.startup_arguments.class_path:
                self.log.warning(
                    "The JVM.startup_arguments.class_path value is ignored when "
                    "using Manager.start_jpype_jvm."
                )
            self.JVM.startup_arguments.class_path = self.jars() + list(extra_jars)
            self.JVM.required()

            java_version = str(jpype.java.lang.System.getProperty('java.version'))
            if not java_version.startswith('1.8') and not java_version.startswith('11.'):
                raise OSError(
                    f'Java version must be either 1.8 or 11, got {java_version}'
                )
        else:
            self.log.info('JVM is already started')

        # If this is the first time we've started the JVM with cmmnbuild-dep-manager
        # register a function with Python to ensure that the JVM is properly shut-down
        # (forcibly if necessary).
        if not getattr(self._terminate_jvm, '_atexit_registered', False):
            atexit.register(self._terminate_jvm)
            # Attach a global attribute to the staticmethod to register the fact
            # that the termination has been registered with atexit.
            self._terminate_jvm._atexit_registered = True

        return jpype

    @staticmethod
    def _terminate_jvm():
        """
        Schedule a function to terminate the JVM by running ``System.exit(0)``
        in a few seconds from execution.

        This is useful in the situation where some code doesn't have full control
        of its threads, and therefore there is no way to cleanly terminate execution.
        This genuinely occurred in JAPC, specifically with "ActiveMQ Transport".
        Once the known JAPC issues are resolved, this method should be removed as
        it should be the responsibility of the developer to ensure any user threads
        are terminated correctly.

        """
        if not jpype.isJVMStarted():
            return

        def jvm_sys_exit():
            # Terminate the underlying JVM.
            return jpype.java.lang.System.exit(0)

        def daemon_thread(runnable):
            thread = jpype.java.lang.Thread(runnable)
            thread.setDaemon(True)
            return thread

        daemonThreadFactory = jpype.JProxy(
            'java.util.concurrent.ThreadFactory',
            {'newThread': daemon_thread},
        )
        task = jpype.JProxy(
            'java.lang.Runnable',
            {'run': jvm_sys_exit},
        )
        executor = jpype.java.util.concurrent.Executors.newSingleThreadScheduledExecutor(
            daemonThreadFactory,
        )

        # In 3 seconds run System.exit(0) in the JVM. As this is a daemon thread
        # executor the task will not block sooner JVM exit, in which case the
        # task will not execute.
        executor.schedule(task, 3, jpype.java.util.concurrent.TimeUnit.SECONDS)

    def _user_dir(self):
        """
        Returns the module directory in the usersitepackages

        Note that over time this private method has evolved to return the
        location of
        ``$HOME/.local/lib/python?.?/site-packages/cmmnbuild_dep_manager`` if
        user site-packages are enabled, otherwise the location of
        cmmnbuild-dep-manager installed inside a virtual environment.

        """

        if hasattr(site, 'getusersitepackages'):
            if site.ENABLE_USER_SITE:
                return os.path.join(site.getusersitepackages(), __package__)
            else:
                # Honour the fact that user-site is disabled, but still return
                # *something*.
                return self._dist_dir()
        else:
            # Virtualenvs used to have no site.getusersitepackages function
            # (https://github.com/pypa/virtualenv/issues/804).
            # Since we are running in VirtualEnv the path in __package__
            # will be absolute.
            return __package__

    def _dist_dir(self):
        '''Returns the module directory in the distribution'''
        return os.path.dirname(__file__)

    def _site_packages(self, assert_writable=False):
        """
        Return a non-user site-packages directory.

        Note that if assert_writable is False, the first non-user site-packages
        directory in ``site.getsitepackages()`` will be returned.

        Parameters
        ----------

        assert_writable:
            If True, validate that the site-packages directory is writable for
            the effective user.

        """
        def is_writable(directory):
            return os.access(
                directory, os.W_OK | os.X_OK,
            )

        for location in site.getsitepackages():
            if location == site.getusersitepackages():
                continue

            if assert_writable and not is_writable(location):
                continue

            return location
        else:
            raise RuntimeError("No writable site-packages directory available")

    def _dir(self):
        """
        Returns a directory for cmmnbuild-dep-manager which is highly likely
        to be writable by the user (though this is not guaranteed).

        The result follows the following strategy:

         * If user site packages enabled, and it exists, return a
           ``.local/lib/python?.?/site-packages/cmmnbuild_dep_manager``
           directory, even if that particular directory doesn't exist and that
           isn't where cmmnbuild_dep_manager is installed.
         * If we are in a virtualenvironment (not a venv), return the install
           location of cmmnbuild_dep_manager.
         * Otherwise return the installed location of cmmnbuild_dep_manager.

        """
        if os.path.isdir(self._user_dir()):
            return self._user_dir()
        else:
            return self._dist_dir()

    def _load_modules(self):
        '''Load modules data from modules.json

        Returns a dictionary of the form:

            {'name': 'version', ...}

        Where 'name' is the module name and 'version' is the version of the
        module for which the jars were downloaded for. Version will be an empty
        string if the module has been registered but not downloaded.
        '''
        modules = {}

        # Load pickled 'modules' files created by versions < 2.0.0
        old_mod_files = (
            os.path.join(self._dist_dir(), 'modules'),
            os.path.join(self._user_dir(), 'modules')
        )
        for f in old_mod_files:
            if os.path.isfile(f):
                self.log.debug('loading {0}'.format(f))
                with open(f, 'rb') as fp:
                    pkl_data = pickle.load(fp)
                    for k in pkl_data:
                        modules[k] = ''

        # Load json 'modules.json' files created by version >= 2.0.0
        mod_files = (
            PKG_MODULES_JSON,
            os.path.join(self._user_dir(), 'modules.json')
        )
        for f in mod_files:
            if os.path.isfile(f):
                self.log.debug('loading {0}'.format(f))
                with open(f, 'r') as fp:
                    json_data = {}
                    try:
                        json_data = json.load(fp)
                    except json.JSONDecodeError:
                        self.log.warning('file {0} is corrupt, defaulting to empty configuration'.format(f))
                    for k, v in json_data.items():
                        modules[k] = v

        # Load any modules registered as an entrypoint.
        for entrypoint in entrypoints.get_group_all('cmmnbuild_dep_manager'):
            # An entrypoint must have the form "{name}={module_name}[{extras}]"
            # but we only care about the {name} and {module_name} (the version
            # for our purposes) part.
            module, version = entrypoint.name, entrypoint.module_name

            # Check to see if the module is installed, and if so, whether
            # the entry-point represents a different version to the one
            # installed. In that case, warn and force an update.
            # Note that this assumes that {version} == {module}.__version__
            # if it isn't, this warning will always trigger (not desirable).
            if modules.get(module) and modules[module] != version:
                self.log.warning(f"{module} is being updated from "
                                 f"{modules[module]} to {version}")
                modules[module] = ""

            # Add the package to the modules dictionary if it isn't already
            # part of the modules definition. If this is the case we haven't
            # seem this module before, and therefore haven't resolved its
            # dependencies.
            modules.setdefault(module, "")

        return modules

    def _save_modules(self, modules):
        '''Save modules data to modules.json'''
        user_dir = self._user_dir()
        dist_dir = self._dist_dir()
        if os.path.isdir(user_dir):
            save_dir = user_dir
        elif os.access(dist_dir, os.W_OK | os.X_OK):
            save_dir = dist_dir
        else:
            self.log.info('creating directory {0}'.format(user_dir))
            os.makedirs(user_dir)
            save_dir = user_dir
        mod_file = os.path.join(save_dir, 'modules.json')
        self.log.debug('saving {0}'.format(mod_file))
        with open(mod_file, 'w') as fp:
            json.dump(modules, fp)

        # Remove 'modules' file used by versions < 2.0.0
        old_modules = os.path.join(save_dir, 'modules')
        if os.path.isfile(old_modules):
            self.log.warning('removing obsolete file {0}'.format(old_modules))
            os.remove(old_modules)

    def _resolvers(self):
        from .resolver import resolvers
        return resolvers()

    def _setup_resolver(self, override=None):
        for resolver in self._resolvers():
            if resolver.is_available() and (override is None or resolver.__name__ == override):
                self._resolver = resolver
                self.log.info('using resolver: {0} ({1})'.format(resolver.__name__, resolver.description))
                break

    def _find_supported_resolvers_for_module(self, module):
        supported_resolvers = []
        for resolver in self._resolvers():
            try:
                deps = list(getattr(module, resolver.dependency_variable))
                if deps is not None:
                    supported_resolvers.append(resolver)
            except:
                pass
        return supported_resolvers

    def _find_supported_resolvers(self):
        """Given the installed modules, return a list of possible resolvers.
        """
        # We start with *all* resolvers, then whittle them down as we see each
        # of the installed modules.
        resolvers = set(self._resolvers())
        for module in self._load_modules():
            supported_resolvers = self._find_supported_resolvers_for_module(module)
            resolvers.intersection_update(supported_resolvers)

        if not resolvers:
            raise ValueError("This environment is not resolvable - there is no resolver that can resolve all of the dependencies.")

        return resolvers

    def is_resolved(self):
        """Determine whether the JARs needed for this environment have been resolved.
        """
        modules = self._load_modules()

        for module_name, module_version in modules.items():
            if module_version == '':
                # An empty version indicates that the module has not been resolved yet
                # (see docs for _load_modules).
                return False
            if module_version != self._module_version(module_name):
                return False
        return True

    def _module_version(self, module_name):
        """Return the version of the package which is installed"""
        module = importlib.import_module(module_name)
        return module.__version__

    def _stubgen_packages(self):
        """Return the Java packages to generate stubs for, as requested by all registered modules."""
        all_stubgen_packages = set()
        # Build the dependency list from all registered modules.
        for module_name in self.registered_modules():
            try:
                module = importlib.import_module(module_name)
                if hasattr(module, '__stubgen_packages__'):
                    all_stubgen_packages.update(module.__stubgen_packages__)
            except ImportError as err:
                self.log.warning(f"{module_name} registered but not importable (message: {str(err)})")
            except Exception as err:
                self.log.warning(str(err))
        return all_stubgen_packages

    @_requires_resolver
    def _find_module_info(self, module_name):
        """Return the version and the Java dependencies for the given package"""
        module = importlib.import_module(module_name)

        # Check __***_deps__ exists on the module. For example for the cbng_web
        # resolver look for something like ``module.__cmmnbuild_deps__``.
        deps = []
        var_name_for_dependencies = self._resolver.dependency_variable
        if hasattr(module, var_name_for_dependencies):
            deps = list(getattr(module, var_name_for_dependencies))

        if not deps:
            supported_resolvers = self._find_supported_resolvers_for_module(module)
            supported_resolvers_explanation = ''
            if supported_resolvers:
                supported_resolvers_explanation = '\nResolvers compatible with this module:\n{}'.format(
                    ', '.join([' - {0} [{1}] - {2}'.format(r.__name__, r.dependency_variable, r.description)
                               for r in supported_resolvers]))
            raise AttributeError('module {0} does not declare {1}, which is needed for {2} ({3}) {4}'
                                 .format(module_name, self._resolver.dependency_variable,
                                         self._resolver.__name__, self._resolver.description,
                                         supported_resolvers_explanation))

        # Check __version__ exists
        version = module.__version__
        return version, deps

    def register(self, *args):
        '''Register one or more modules'''
        ret = []
        modules = self._load_modules()
        for name in args:
            try:
                version = self._module_version(name)

                if name not in modules or modules[name] != version:
                    modules[name] = ''
                    self.log.info('{0} registered'.format(name))
                    ret.append(name)
            except ImportError:
                self.log.error(
                    '{0} not found'.format(name)
                )
            except Exception as e:
                self.log.error(e)
        if ret:
            self._save_modules(modules)
        return tuple(ret)

    def unregister(self, *args):
        '''Unregister one or more modules'''
        ret = []
        modules = self._load_modules()
        for name in args:
            if name in modules.keys():
                del modules[name]
                self.log.info('{0} unregistered'.format(name))
                ret.append(name)
        if ret:
            self._save_modules(modules)
        return tuple(ret)

    def install(self, *args):
        '''Register one or more modules and resolve dependencies'''
        ret = self.register(*args)
        if ret:
            self.resolve()
        return ret

    def uninstall(self, *args):
        '''Unregister one or more modules and resolve dependencies'''
        ret = self.unregister(*args)
        if ret:
            self.resolve()
        return ret

    def is_registered(self, name):
        '''Check if module is registered'''
        modules = self._load_modules()
        return name in modules.keys()

    def is_installed(self, name, version=None):
        """Check if module is installed (registered and resolved).

        If no version is passed then the version that comes from the imported
        module's ``__version__`` attribute will be used.

        """
        modules = self._load_modules()
        if name in modules:
            if version is None:
                version = self._module_version(name)
            return modules[name] == version
        return False

    def list(self):
        """DEPRECATED"""
        self.log.warning(
            "The Manager.list method is deprecated, please use "
            "Manager.registered_modules instead."
        )
        return self.registered_modules()

    def registered_modules(self):
        """Returns a list of the currently registered Python modules"""
        return sorted(self._load_modules().keys())

    @_requires_resolver
    def java_requirements_for(self, module_name: str) -> typing.Tuple[DependencyType, ...]:
        """Return the Java dependencies for the given module name

        Note that this function can raise:

            ImportError - when the module isn't importable
            AttributeError - when the module doesn't correctly declare its dependencies for this type of resolver

        """
        module = importlib.import_module(module_name)

        dep_var = self._resolver.dependency_variable
        if not hasattr(module, dep_var):
            raise AttributeError(
                f"{module_name} doesn't declare a {dep_var} attribute, "
                f"which is needed by the {self._resolver.__name__} "
                "resolver to get its required Java dependencies"
            )
        return tuple(getattr(module, dep_var))

    @_requires_resolver
    def java_requirements(self) -> typing.Tuple[DependencyType, ...]:
        """Return the Java dependencies for registered modules.

        Unlike :meth:`.java_requirements_for`, this method does not raise
        (except in unhandled cases), instead it logs warnings for
        ``ImportError`` and ``AttributeError``.

        """
        all_dependencies: typing.Tuple[DependencyType] = ()
        # Build the dependency list from all registered modules.
        for module_name in self.registered_modules():
            try:
                all_dependencies += self.java_requirements_for(module_name)
            except ImportError as err:
                self.log.warning(
                    f"{module_name} registered but not importable (message: {str(err)})")
            except AttributeError as err:
                self.log.warning(str(err))
        return all_dependencies

    @_requires_resolver
    def resolve(self, force_resolver=None):
        '''Resolve dependencies for all registered modules using CBNG'''
        if force_resolver is not None:
            self._setup_resolver(force_resolver)
        self.log.info('Resolving JARs for this environment')
        self.log.debug('lib directory is {0}'.format(self.jar_path()))

        modules = self._load_modules()

        all_dependencies = ()

        # Build the dependency list from all installed packages
        for name, vers in list(modules.items()):
            try:
                version = self._module_version(name)
                all_dependencies += self.java_requirements_for(name)
                modules[name] = version
            except ImportError:
                self.log.error('{0} not found'.format(name))
                self.unregister(name)
                modules.pop(name)
            except AttributeError as err:
                self.log.error(str(err))
                self.unregister(name)
                modules.pop(name)
            except Exception as e:
                # Unknown exception (occurring inside the module itself during import).
                self.log.error(e)
                self.unregister(name)
                modules.pop(name)

        self._save_modules(modules)

        try:
            # shutdown JVM to be able to replace the JARs (on windows at least)
            import jpype
            jpype.shutdownJVM()
        except Exception:
            pass

        if not all_dependencies:
            self.log.error('no dependencies were found')
            return

        resolver = self._resolver(all_dependencies)
        self.log.info('resolved {0} using {1}'.format(all_dependencies, resolver))

        # Create user directory if dist directory is not writeable
        if not os.access(self._dist_dir(), os.W_OK | os.X_OK):
            if not os.path.exists(self._user_dir()):
                self.log.info('creating directory {0}'.format(
                    self._user_dir())
                )
                os.makedirs(self._user_dir())

        # ensure the jar destination directory exists
        if not os.path.exists(self.jar_path()):
            self.log.info('creating JAR directory {0}'.format(
                self.jar_path())
            )
            os.makedirs(self.jar_path())

        # Remove 'jars' directory used by versions < 2.0.0
        old_jars_dir = os.path.join(self._dir(), 'jars')
        if os.path.isdir(old_jars_dir):
            self.log.warning('removing obsolete directory {0}'.format(
                old_jars_dir
            ))
            shutil.rmtree(old_jars_dir)

        # Deploy new jars
        resolver.save_jars(self.jar_path())

    @_requires_resolver
    def lockfile_generate(self, lockfile_path: typing.Optional[str] = None):
        """
        Using all of the registered modules, generate a file containing a resolved
        set of artifacts for later installation.

        See also :meth:`lockfile_install`.

        Parameters
        ----------

        lockfile_path:
            The path of the lockfile to install.
            Defaults to ``cmmnbuild-dep-manager.lock`` in the CWD.


        Note that, unlike :meth:`.resolve` this method can raise in the
        case of incorrectly installed packages.

        """
        from .resolver import MultiPhaseResolver

        if not issubclass(self._resolver, MultiPhaseResolver):
            raise TypeError(
                f"Only multi-phase resolvers such as CbngWebResolver can be "
                f"used to generate a lockfile. "
                f"The resolver enabled is {self._resolver}."
            )

        if lockfile_path is None:
            lockfile_path = pathlib.Path.cwd() / 'cmmnbuild-dep-manager.lock'
        else:
            lockfile_path = pathlib.Path(lockfile_path)

        self.log.info('Resolving JARs for this environment')

        # Run the resolver, possibly raising if there are any issues with the
        # modules, or the resolution itself.
        java_reqs = self.java_requirements()
        resolver = self._resolver(java_reqs)

        # Get the names and versions of the modules which are being
        # considered for JARs.
        mod_versions = {
            module_name: self._module_version(module_name)
            for module_name in self.registered_modules()
        }

        with lockfile_path.open('wt') as fh:
            json.dump(
                {
                    'python_modules': mod_versions,
                    'java_requirements': java_reqs,
                    'resolved_artifacts':
                        [dataclasses.asdict(artifact)
                         for artifact in resolver.resolved_artifacts],
                },
                fh,
                indent=2,
            )
        self.log.info(f'Lockfile created at {lockfile_path}')

    @_requires_resolver
    def lockfile_install(self, lockfile_path: typing.Optional[str] = None):
        """
        Install the lockfile by downloading the necessary JARs and marking the
        environment as resolved.

        Parameters
        ----------

        lockfile_path:
            The path of the lockfile to install.
            Defaults to ``cmmnbuild-dep-manager.lock`` in the CWD.

        """
        from .resolver import MultiPhaseResolver, Artifact

        if not issubclass(self._resolver, MultiPhaseResolver):
            raise TypeError(
                f"Only multi-phase resolvers such as CbngWebResolver can be "
                f"used to generate a lockfile. "
                f"The resolver enabled is {self._resolver}."
            )

        if lockfile_path is None:
            lockfile_path = pathlib.Path.cwd() / 'cmmnbuild-dep-manager.lock'
        else:
            lockfile_path = pathlib.Path(lockfile_path)

        with lockfile_path.open('rt') as fh:
            lockfile_contents = json.load(fh)

        artifacts = tuple(
            Artifact(**artifact) for artifact in lockfile_contents['resolved_artifacts']
        )

        def to_requirements_set(requirements):
            return {
                tuple(req.items()) if isinstance(req, dict) else req
                for req in requirements
            }

        # Check that the environment's Java requirements match the ones in the file.
        # Note that this check does not behave like the main `Manager.resolve() method
        # as that method checks the *python* modules are the same. At the end of the
        # day though, all we really care about is satisfying the Java dependencies,
        # and if the Python module version changes in between locking and installation,
        # we don't need that to be a concern so long as the Java requirements remain the
        # same.
        env_java_reqs = to_requirements_set(self.java_requirements())
        lockfile_reqs = to_requirements_set(lockfile_contents['java_requirements'])
        if lockfile_reqs != env_java_reqs:
            raise ValueError(
                "The Java requirements from the lockfile do not match those for the environment: \n"
                f"   Lockfile: \n{pprint.pformat(lockfile_reqs, indent=5)}\n"
                f"   Environment: \n{pprint.pformat(env_java_reqs, indent=5)}\n"
            )

        destination = pathlib.Path(self.jar_path())
        destination.mkdir(exist_ok=True, parents=True)

        self._resolver.download_jars(destination, artifacts)
        self._save_modules(lockfile_contents['python_modules'])

    def stubgen(self, output_dir=None):
        """ Generate python type stubs for accessing Java APIs through the JPype import system (see imports()).

        If output_dir is not specified, the stubs will be placed in the directory where cmmnbuild_dep_manager is
        installed (this directory has to be writable).

        For IDEs like PyCharm and linters like MyPy to pick up the stubs, this directory must be on PYTHONPATH (or
        a special directory where the tool in question expects the stubs).
        """
        try:
            import stubgenj
        except ImportError:
            self.log.error(
                "Please install the stubgen optional extra for cmmnbuild-dep-manager"
                "to be able to generate stubs (pip install cmmnbuild-dep-manager[stubgen])"
            )
            return

        unique_packages = sorted(set(self._stubgen_packages()))
        if not unique_packages:
            self.log.info('No packages registered for stub generation')
            return

        self.log.info('generating stubs for {0} package trees'.format(len(unique_packages)))

        if output_dir is None:
            output_dir = pathlib.Path(self._site_packages(assert_writable=True))

        with self.imports():
            imported_packages = [importlib.import_module(pkg) for pkg in unique_packages]
            stubgenj.generateJavaStubs(imported_packages, useStubsSuffix=True, outputDir=output_dir)
        self.log.info(f"Generated stubs available at {output_dir}")
